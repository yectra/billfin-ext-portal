import { GetterTree, MutationTree, ActionTree } from 'vuex';

import { UserState, UserModel } from '@/model';
import { IPortalService } from '@/service';

const state: UserState = {
    userName: "",
    firmName: "",
    avatar: undefined
}

const getters: GetterTree<UserState, any> = {
    userName: state => {
        return state.userName;
    },
    firmName: state => {
        return state.firmName;
    },
    avatar: state => {
        return state.avatar;
    }
}

const mutations: MutationTree<UserState> = {
    onGetUser(state, data: UserModel) {
        state.userName = data.userName;
        state.firmName = data.firmName;
        state.avatar = data.avatar;
    },
}

const actions: ActionTree<UserState, any> = {
    getUser(context, service: IPortalService) {
        return service.getUser().then(response => {
            context.commit('onGetUser', response);

            return response;
        });
    }
}

export const UserModule = {
    state,
    getters,
    mutations,
    actions
}