import { Vue, Provide } from "vue-property-decorator";

import { IPortalService, PortalService, PortalMockService } from '@/service';


export class DIContainer extends Vue {
    @Provide('portalService') portalService: IPortalService = new PortalMockService();
}