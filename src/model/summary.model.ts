export class SummaryModel {
    public date: Date;

    public totalAccounts: number;
    public totalCredits: number;
    public totalDebits: number;
    public net: number;
}