export class UploadResponse {
    errors: Array<string>;
    totalCreditAccounts: number;
    totalCreditAmount: number;
    totalDebitAccounts: number;
    totalDebitAmount: number;
    totalInsufficientFund: number;
    totalNetFunds: number;

    credits: Array<FeeModel>;
    debits: Array<FeeModel>;
    insufficientFunds: Array<FeeModel>;
}

export class FeeModel {
    accountNumber: string;
    feeAmount: number;
    feeDescription: string;
    availableFund: number;
    shortFall: number;
    errorDescription: string;
}
