
export class UserModel {
    userName: string = "";
    firmName: string = "";
    avatar: string = undefined;
}

export class UserState {
    userName: string = "";
    firmName: string = "";
    avatar: string = undefined;
}
