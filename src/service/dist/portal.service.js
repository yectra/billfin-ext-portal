"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
exports.PortalMockService = exports.PortalService = void 0;
var model_1 = require("@/model");
var base_service_1 = require("./base.service");
var PortalService = /** @class */ (function (_super) {
    __extends(PortalService, _super);
    function PortalService() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    PortalService.prototype.getUser = function () {
        return this.httpGet("admin/userDetails", null).then(function (response) {
            return response.data;
        });
    };
    PortalService.prototype.getLastUploaded = function () {
        return this.httpGet("billing/getLastUploadedManualFees", null).then(function (response) {
            return response.data;
        });
    };
    PortalService.prototype.uploadFee = function (file) {
        var formData = new FormData();
        formData.append("manualFee", file);
        return this.upload("billing/manualFeeUpload", formData).then(function (response) {
            return response.data;
        });
    };
    PortalService.prototype.submitFee = function (request) {
        return this.httpPost("billing/submitManualFee", null).then(function (response) {
            return response.data;
        });
    };
    return PortalService;
}(base_service_1.ServiceHelper));
exports.PortalService = PortalService;
var PortalMockService = /** @class */ (function () {
    function PortalMockService() {
    }
    PortalMockService.prototype.getUser = function () {
        return new Promise(function (resolve, reject) {
            var user = new model_1.UserModel();
            user.userName = "Gerry";
            user.firmName = "Klingman & Associates, LLC.";
            resolve(user);
        });
    };
    PortalMockService.prototype.getLastUploaded = function () {
        return new Promise(function (resolve, reject) {
            var summary = new model_1.SummaryModel();
            summary.date = new Date();
            summary.totalAccounts = 425;
            summary.totalDebits = 12538;
            summary.totalCredits = 2481;
            summary.net = 10057;
            resolve(summary);
        });
    };
    PortalMockService.prototype.uploadFee = function (file) {
        return new Promise(function (resolve, reject) {
            var response = new model_1.UploadResponse();
            // response.errors = ["File contains one or more invalid account numbers."];
            // response.errors.push("43r42, 4244K are not valid accounts");
            response.totalCreditAccounts = 2;
            response.totalCreditAmount = 1651.56;
            response.totalDebitAccounts = 2;
            response.totalDebitAmount = 1135.56;
            response.totalInsufficientFund = 130.0;
            response.totalNetFunds = 22508.83;
            response.credits = [];
            response.debits = [];
            response.insufficientFunds = [];
            var debit = new model_1.FeeModel();
            debit.accountNumber = "123456";
            debit.feeAmount = 3786.91;
            debit.availableFund = 142907.52;
            response.debits.push(debit);
            response.debits.push(debit);
            response.debits.push(debit);
            var credit = new model_1.FeeModel();
            credit.accountNumber = "123456";
            credit.feeAmount = 3786.91;
            credit.availableFund = 142907.52;
            response.credits.push(credit);
            response.credits.push(credit);
            response.credits.push(credit);
            response.credits.push(credit);
            response.credits.push(credit);
            var insufficient = new model_1.FeeModel();
            insufficient.accountNumber = "123456";
            insufficient.feeAmount = 3786.91;
            insufficient.availableFund = 142907.52;
            insufficient.shortFall = 3335;
            response.insufficientFunds.push(insufficient);
            response.insufficientFunds.push(insufficient);
            response.insufficientFunds.push(insufficient);
            response.insufficientFunds.push(insufficient);
            response.insufficientFunds.push(insufficient);
            response.insufficientFunds.push(insufficient);
            response.insufficientFunds.push(insufficient);
            resolve(response);
        });
    };
    PortalMockService.prototype.submitFee = function (request) {
        return new Promise(function (resolve, reject) {
            resolve(true);
        });
    };
    return PortalMockService;
}());
exports.PortalMockService = PortalMockService;
