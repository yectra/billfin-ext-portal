import axios from 'axios';
import { AxiosResponse, AxiosRequestConfig, ResponseType } from 'axios'
import { stringify } from 'qs';

import { Settings } from '@/config';

export abstract class ServiceHelper {
    protected baseUrl: string = Settings.ApiUrl;

    protected apiUrl: string = `${this.baseUrl}/${Settings.ApiPath}`;

    httpGet(route: string, request: any, responseType?: ResponseType): Promise<AxiosResponse<any>> {
        let params: any = {};
        if (request) {
            for (let key in request) {
                if (key.charAt(0) != "_") {
                    if (request[key] instanceof Date) {
                        let dateString: any = request[key];

                        let date: Date = new Date(dateString);

                        if (date)
                            params[key] = `${date.getFullYear()}-${("0" + (date.getMonth() + 1)).slice(-2)}-${("0" + date.getDate()).slice(-2)}`;
                    }
                    else {
                        let value: any = request[key];

                        if (value || value === false)
                            params[key] = request[key];
                    }
                }
            }
        }

        let path = `${this.apiUrl}/${route}`;
        let config: AxiosRequestConfig = {
            params: params,
            'paramsSerializer': params => {
                return stringify(params, { arrayFormat: 'repeat' })
            }
        };

        if (responseType) config.responseType = responseType;

        return axios.get<any>(path, config);
    }


    protected httpPost(route: string, data: any): Promise<AxiosResponse<any>> {
        let path = `${this.apiUrl}/${route}`;

        return axios.post(path, data);
    }

    protected httpPut(route: string, data: any): Promise<AxiosResponse<any>> {
        let path = `${this.apiUrl}/${route}`;

        return axios.put(path, data);
    }

    protected httpDelete(route: string): Promise<AxiosResponse<any>> {
        let path = `${this.apiUrl}/${route}`;

        return axios.delete(path);
    }

    protected upload(route: string, formData: FormData) {
        let path = `${this.apiUrl}/${route}`;

        let config: AxiosRequestConfig = { headers: { 'Content-Type': 'multipart/form-data' } };

        return axios.post(`${this.apiUrl}/${path}`, formData, config).then(response => {
            return response.data;
        });
    }
}