import { UserModel, SummaryModel, UploadResponse, FeeModel } from '@/model';
import { ServiceHelper } from './base.service';

export interface IPortalService {
    getUser(): Promise<UserModel>;
    getLastUploaded(): Promise<SummaryModel>;

    uploadFee(file: File): Promise<UploadResponse>;
    uploadAvatar(file: File): Promise<boolean>;
    submitFee(request: UploadResponse): Promise<boolean>;
    saveUserName(name: string): Promise<boolean>;
}

export class PortalService extends ServiceHelper implements IPortalService {

    getUser(): Promise<UserModel> {
        return this.httpGet("admin/userDetails", null).then(response => {
            return response.data;
        });
    }

    getLastUploaded(): Promise<SummaryModel> {
        return this.httpGet("billing/getLastUploadedManualFees", null).then(response => {
            return response.data;
        });
    }

    uploadFee(file: File): Promise<UploadResponse> {
        let formData = new FormData();
        formData.append("manualFee", file);

        return this.upload("billing/manualFeeUpload", formData).then(response => {
            return response.data;
        });
    }

    submitFee(request: UploadResponse): Promise<boolean> {
        return this.httpPost("billing/submitManualFee", null).then(response => {
            return response.data;
        });
    }

    saveUserName(name: string): Promise<boolean> {
        return this.httpPost("admin/saveUser", { name: name }).then(response => {
            return response.data;
        });
    }

    uploadAvatar(file: File): Promise<boolean> {
        let formData = new FormData();
        formData.append("image", file);

        return this.upload("admin/uploadAvatar", formData).then(response => {
            return response.data;
        });
    }
}

export class PortalMockService implements IPortalService {
    getUser(): Promise<UserModel> {
        return new Promise((resolve, reject) => {
            let user = new UserModel();
            user.userName = "Gerry";
            user.firmName = "Klingman & Associates, LLC.";

            resolve(user);
        });
    }

    getLastUploaded(): Promise<SummaryModel> {
        return new Promise((resolve, reject) => {
            let summary = new SummaryModel();

            summary.date = new Date();
            summary.totalAccounts = 425;
            summary.totalDebits = 12538;
            summary.totalCredits = 2481;
            summary.net = 10057;

            resolve(summary);
        });
    }

    uploadFee(file: File): Promise<UploadResponse> {
        return new Promise((resolve, reject) => {
            let response = new UploadResponse();

            // response.errors = ["File contains one or more invalid account numbers."];
            // response.errors.push("43r42, 4244K are not valid accounts");

            response.totalCreditAccounts = 2;
            response.totalCreditAmount = 1651.56;
            response.totalDebitAccounts = 2;
            response.totalDebitAmount = 1135.56;
            response.totalInsufficientFund = 130.0;

            response.totalNetFunds = 22508.83;

            response.credits = [];
            response.debits = [];
            response.insufficientFunds = [];

            let debit = new FeeModel();
            debit.accountNumber = "123456";
            debit.feeAmount = 3786.91;
            debit.availableFund = 142907.52;

            response.debits.push(debit);
            response.debits.push(debit);
            response.debits.push(debit);

            let credit = new FeeModel();
            credit.accountNumber = "123456";
            credit.feeAmount = 3786.91;
            credit.availableFund = 142907.52;

            response.credits.push(credit);
            response.credits.push(credit);
            response.credits.push(credit);
            response.credits.push(credit);
            response.credits.push(credit);

            let insufficient = new FeeModel();
            insufficient.accountNumber = "123456";
            insufficient.feeAmount = 3786.91;
            insufficient.availableFund = 142907.52;
            insufficient.shortFall = 3335;

            response.insufficientFunds.push(insufficient);
            response.insufficientFunds.push(insufficient);
            response.insufficientFunds.push(insufficient);
            response.insufficientFunds.push(insufficient);
            response.insufficientFunds.push(insufficient);
            response.insufficientFunds.push(insufficient);
            response.insufficientFunds.push(insufficient);


            resolve(response);
        });
    }

    submitFee(request: UploadResponse): Promise<boolean> {
        return new Promise((resolve, reject) => {
            resolve(true);
        });
    }

    saveUserName(name: string): Promise<boolean> {
        return new Promise((resolve, reject) => {
            resolve(true);
        });
    }

    uploadAvatar(file: File): Promise<boolean> {
        return new Promise((resolve, reject) => {
            resolve(true);
        });
    }
}