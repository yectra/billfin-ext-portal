module.exports = {
  chainWebpack: config => {
    // CSV Loader
    config.module
      .rule('csv')
      .test(/\.(csv|xlsx|xls)$/)
      .use('file-loader')
        .loader('file-loader')
        .end()
  }
}